package com.kata.stringCalculator.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {StringCalculator.class})
public class StringCalculatorTest {

    StringCalculator stringCalculator = new StringCalculator();

    @Test
    public void add_withNumbers_shouldReturnFour() {
        assertEquals(4, stringCalculator.add("2,2"));
    }

    @Test
    public void add_withEmptyString_shouldReturnZero() {
        assertEquals(0, stringCalculator.add(""));
    }

    @Test
    public void add_withMoreThanTwoNumbers_shouldReturnCorrectResult() {
        assertEquals(24, stringCalculator.add("10,8,2,4"));
    }

    @Test
    public void add_withNEwLines_shouldReturnCorrectResult() {
        assertEquals(18, stringCalculator.add("10,2\n6"));
    }

    @Test
    public void add_withSpecialCharacter_shouldReturnCorrectResult() {
        assertEquals(16, stringCalculator.add("//;\n4;10;2"));
    }

    @Test
    public void add_withOtherSpecialCharacter_shouldReturnCorrectResult() {
        assertEquals(16, stringCalculator.add("//%\n4%10,2"));
    }


    @Test
    public void add_withNegativeNumber_shouldReturnCorrectResult() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->  stringCalculator.add("-4,-8,5,5"));
        assertEquals(exception.getMessage(), "negatives not allowed : -4, -8");
    }

    @Test
    public void add_withOtherNegativeNumber_shouldReturnCorrectResult() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->  stringCalculator.add("-12,-7,2"));
        assertEquals(exception.getMessage(), "negatives not allowed : -12, -7");
    }
    
}
