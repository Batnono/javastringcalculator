package com.kata.stringCalculator.service;

import java.util.List;
import java.util.stream.Collectors;

public class StringCalculator {

    public Integer add(String stringInput) {
        String stringNumbers = new String(stringInput);
        if(stringInput.isEmpty()) {
            return 0;
        }
        if(stringNumbers.contains("//")) {
            String delimiter = stringNumbers.split("\n")[0].replace("//", "");
            stringNumbers = stringNumbers.split("\n")[1].replace(delimiter, ",");
        }
        List<Integer> numbers = List.of(stringNumbers.replace("\n", ",").split(",")).stream().map(str -> Integer.parseInt(str)).toList();
        if(stringNumbers.contains("-")) {
            throw new IllegalArgumentException("negatives not allowed : " + numbers.stream()
            .filter(nbr -> nbr < 0).map(nbr -> nbr.toString()).collect(Collectors.joining(", ")));
        }
        return numbers.stream().mapToInt(Integer::intValue).sum();
    }
    
}
